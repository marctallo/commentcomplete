<?php

use App\Credential;

use Illuminate\Database\Seeder;

class CredentialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $credentials = [
            "PE",
            "MBA",
            "FPE",
            "EIT",
            "MS"
        ];

        foreach ($credentials as $value) {

            Credential::create([
                'name' => $value,
            ]);
        }


    }
}
