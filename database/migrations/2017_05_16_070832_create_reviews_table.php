<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client_email')->nullable();
            $table->string('client_name');
            $table->string('client_company');
            $table->text('client_address');
            $table->string('project_title');
            $table->string('project_location');
            $table->string('project_iteration');
            $table->string('project_number');
            $table->string('salutation');
            $table->text('general_info');
            $table->text('closing_remarks')->nullable();
            $table->string('complimentary_closing');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
