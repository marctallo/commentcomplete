<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone_number' => $faker->phoneNumber,
        'company' => $faker->company,
        'company_address' => $faker->address,
        'credentials' => $faker->randomElement(["PE","MBA","FPE","EIT","MS"])
    ];
});



$factory->define(App\Review::class, function (Faker\Generator $faker) {

    $client_name = $faker->name();

    return [
        'client_email' => $faker->safeEmail,
        'client_name' => $client_name,
        'client_company' => $faker->company,
        'client_address' => $faker->address,
        'project_title' => $faker->sentence,
        'project_location' => $faker->city,
        'project_iteration' => $faker->sentence,
        'project_number' => $faker->randomNumber(5),
        'salutation' => "Dear ".$client_name,
        'general_info' => $faker->paragraph,
        'closing_remark' => $faker->sentence,
        'complimentary_closing' => "Best Regards",
        'user_id' => factory(App\User::class)->create()->id
    ];
});

$factory->define(App\Comment::class, function (Faker\Generator $faker) {

    $client_name = $faker->name();

    return [
        'comment' => $faker->paragraph,
        'code' => $faker->word,
        'section' => $faker->word,
        'user_id' => factory(App\User::class)->create()->id
    ];
});
