<?php

namespace Tests\Itegration\Repositories;

use App\User;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use App\Repositories\UserRepository;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserRepositoryTest extends TestCase
{

    use DatabaseMigrations;

    /** @test */
    public function it_creates_a_new_user()
    {

         $uploader = \Mockery::mock('App\Libraries\Upload\CompanyLogoUploader');

         $user = new UserRepository($uploader);

         $created = $user->create([

            "username" => "test",
            "email" => "test@email.com",
            "password" => "password"
         ]);

         $this->assertDatabaseHas("users",[
             "username" => "test"
         ]);

         $this->assertInstanceOf(User::class, $created);
    }

    /** @test */
    public function it_updates_current_user()
    {

        $user = factory(User::class)->create();

        $file = UploadedFile::fake()->image('avatar.jpg');

        $uploader = \Mockery::mock('App\Libraries\Upload\CompanyLogoUploader');

        $uploader->shouldReceive('handle')->with($file,$user)->once()->andReturn("company_logo.jpg");

        $user_repo = new UserRepository($uploader);

        $user_repo->update([

            "first_name" => "test firstname",
            "last_name" => "test lastname",
            "company_logo" => $file
        ],$user);

        $this->assertDatabaseHas("users",[
            "first_name" => "test firstname",
            "last_name" => "test lastname",
        ]);

        $this->assertEquals("company_logo.jpg",$user->company_logo);


    }

}
