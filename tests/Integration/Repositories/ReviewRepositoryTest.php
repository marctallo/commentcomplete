<?php

namespace Tests\Integration\Repositories;

use App\User;
use App\Review;
use App\Comment;
use Tests\TestCase;
use App\CommentReview;
use App\Repositories\ReviewRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReviewRepositoryTest extends TestCase
{

    use DatabaseMigrations;

    protected $user;

    protected $comment;

    protected $review;

    protected $review_repo;

    protected $dummy_comments;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->comment = factory(Comment::class)->make();

        $this->review = factory(Review::class)->make();

        $this->review_repo = new ReviewRepository(new Review , new Comment);

        $this->dummy_comments = ["comment 1","comment 2","comment 3"];
    }

    /** @test */
    public function it_should_create_a_new_review()
    {


        $data = $this->makeDummyData();

        $review = $this->review_repo->create($data , $this->user);

        $this->assertCount(1,Review::all());

        $this->assertCount(1,Comment::all());

        $this->assertCount(1,$review->comments);

    }

    /** @test */
    public function it_should_create_a_new_review_without_comment()
    {
        $data = $this->makeDummyData();

        unset($data["comment"]);

        $review = $this->review_repo->create($data , $this->user);

        $this->assertCount(1,Review::all());

        $this->assertCount(0,Comment::all());

    }

    /** @test */
    public function it_should_create_review_with_multiple_comments()
    {

        $data = $this->makeDummyData();

        $data["comment"] = $this->dummy_comments;

        $review = $this->review_repo->create($data , $this->user);

        $this->assertCount(3,$review->comments);

    }

    /** @test */
    public function it_should_update_a_review()
    {
        $data = $this->makeDummyData();

        $data["comment"] = $this->dummy_comments;

        $review = $this->review_repo->create($data , $this->user);

        $this->review_repo->update(["project_title" => "Test Project Title","comment" => $this->dummy_comments] , $review , $this->user);

        $this->assertEquals("Test Project Title",Review::first()->project_title);

        $this->assertCount(3,CommentReview::all());
    }

    /** @test */
    public function it_should_delete_a_review()
    {
        $data = $this->makeDummyData();

        $data["comment"] = $this->dummy_comments;

        $review = $this->review_repo->create($data , $this->user);

        $deleted = $this->review_repo->delete($review);

        $this->assertCount(0,Review::all());

        $this->assertTrue($deleted);

        //disabled because of sqlite config not supporting cascade deletes
        //can find fix here http://novate.co.uk/supporting-delete-cascade-with-sqlite-and-laravel/
        //$this->assertCount(0,CommentReview::all());
    }

    private function makeDummyData()
    {

        return [
            'client_email' => $this->review->client_email,
            'client_name' => $this->review->client_name,
            'client_company' => $this->review->client_company,
            'client_address' => $this->review->client_address,
            'project_title' => $this->review->project_title,
            'project_location' => $this->review->project_location,
            'project_iteration' => $this->review->project_iteration,
            'project_number' => $this->review->project_number,
            'salutation' => $this->review->salutation,
            'general_info' => $this->review->general_info,
            'closing_remark' => $this->review->closing_remark,
            'complimentary_closing' => $this->review->complimentary_closing,
            'comment' => ["comment" => $this->comment->comment]
        ];
    }
}
