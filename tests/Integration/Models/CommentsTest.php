<?php

namespace Tests\Integration\Model;

use App\User;
use App\Comment;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommentsTest extends TestCase
{

    use DatabaseMigrations;

    protected $comment1;

    protected $comment2;

    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->comment1 = factory(Comment::class)->make();

        $this->user = factory(User::class)->create();
    }

    /** @test */
    public function it_creates_a_new_comment()
    {

        $comment = Comment::create($this->comment1->comment, $this->user);

        $this->assertInstanceOf(Comment::class,$comment);

        $this->assertCount(1 , Comment::all());
    }

    /** @test */
    public function it_create_a_new_comment_that_exists()
    {

        $comment = factory(Comment::class)->create();

        $new_comment = Comment::create($comment->comment,$this->user);

        $this->assertCount(1 , Comment::all());
    }

}
