<?php

namespace Tests\Integration\Models;

use App\Credential;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CredentialsTest extends TestCase
{

    use DatabaseMigrations;

    /** @test */
    public function it_creates_an_array_for_dropdown()
    {

        $credentials = Credential::dropDown();

        $this->assertGreaterThan(0,count($credentials));
        $this->assertInternalType('array',$credentials);
    }
}
