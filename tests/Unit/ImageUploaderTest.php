<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Libraries\Upload\ImageUploader;
use App\Libraries\Upload\CompanyLogoUploader;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImageUploaderTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_should_upload_a_company_logo()
    {

        Storage::fake('uploads');

        $user = factory(User::class)->create();

        $uploader = new ImageUploader();

        $filename = $uploader->handle(UploadedFile::fake()->image('avatar.jpg'),$user , new CompanyLogoUploader);

        Storage::disk('uploads')->assertExists($filename);
    }
}
