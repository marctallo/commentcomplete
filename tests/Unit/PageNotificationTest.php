<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Libraries\Notification\PageNotification;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PageNotificationTest extends TestCase
{

    /** @test */
    public function a_notification_is_triggered()
    {

        $notification = new PageNotification;

        $notification->show("error","This is a test message");

        $this->assertTrue(session("show_notification"));

        $this->assertEquals("error",session("notification.type"));

    }
}
