<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterTest extends DuskTestCase
{

    use DatabaseMigrations;

    protected $user;

    protected $password;

    protected $registered_user;

    public function setUp()
    {
        parent::setUp();


        $this->user = factory(User::class)->make();

        $this->registered_user = factory(User::class)->create();

        $this->password = uniqid();

    }

    /** @test */
    public function a_user_registers()
    {

        $this->browse(function (Browser $browser) {

            $browser->visit('register')
                    ->type("username",$this->user->username)
                    ->type("email",$this->user->email)
                    ->type("password",$this->password)
                    ->type("password_confirmation",$this->password)
                    ->press("REGISTER")
                    ->assertPathIs("/review");

            $this->assertDatabaseHas("users",[
                "email" => $this->user->email
            ]);
        });
    }


    /** @test */
    public function a_user_register_with_existing_credentials()
    {

        $this->browse(function (Browser $browser) {

            $browser->visit("register")
                    ->type("username",$this->registered_user->username)
                    ->type("email",$this->registered_user->email)
                    ->type("password",$this->password)
                    ->type("password_confirmation",$this->password)
                    ->press("REGISTER")
                    ->assertVisible(".error-msg");
        });

    }

}
