<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserUpdateTest extends DuskTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function user_updates_account_info()
    {

        $user = factory(User::class)->create();



        $this->browse(function (Browser $browser) use ($user) {

            $browser->loginAs($user)
                    ->visit("/account")
                    ->type("username","testusername")
                    ->type("email","testemail@email.com")
                    ->press("Update");

            $updated_user = User::first();

            $this->assertEquals("testusername",$updated_user->username);
        });
    }

    /** @test */
    public function user_updates_password()
    {

        $user = factory(User::class)->create([

            "password" => "oldpassword"
        ]);

        $this->browse(function (Browser $browser) use ($user) {

            $browser->loginAs($user)
                    ->visit("/account")
                    ->clickLink("Password")
                    ->type("old_password","oldpassword")
                    ->type("password","newpassword")
                    ->type("password_confirmation","newpassword")
                    ->press("Change Password");

            $updated_user = User::first();

            $password = Hash::check('newpassword', $updated_user->password);

            $this->assertTrue($password);
        });

    }

    /** @test */
    public function user_updates_company_info()
    {

        $user = factory(User::class)->create();

        $this->browse(function (Browser $browser) use ($user) {

            $browser->loginAs($user)
                    ->visit("/account")
                    ->clickLink("Company")
                    ->type("company","Test Company")
                    ->type("company_address","Test Address")
                    ->select("credentials")
                    ->attach("company_logo","Z:/tests/789.jpg")
                    ->press("Update");

            $updated_user = User::first();

            $file_exists = Storage::disk("uploads")->exists($updated_user->company_logo);

            $this->assertTrue($file_exists);
        });

    }
}
