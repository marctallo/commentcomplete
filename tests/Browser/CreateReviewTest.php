<?php

namespace Tests\Browser;

use App\User;
use App\Review;
use App\Comment;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateReviewTest extends DuskTestCase
{
    use DatabaseMigrations;


    protected $user;

    protected $review;

    protected $comment;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->review = factory(Review::class)->make();

        $this->comment = factory(Comment::class)->make();

    }
    /** @test */
    public function a_user_create_a_new_review()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user)
                    ->visit("/review")
                    ->clickLink("New Review")
                    ->type("client_email",$this->review->client_email)
                    ->type("client_name",$this->review->client_name)
                    ->type("client_company",$this->review->client_company)
                    ->type("client_address",$this->review->client_address)
                    ->type("project_title",$this->review->project_title)
                    ->type("project_location",$this->review->project_location)
                    ->type("project_iteration",$this->review->project_iteration)
                    ->type("salutation",$this->review->salutation)
                    ->type("general_info",$this->review->general_info)
                    ->type("comment",$this->comment->comment)
                    ->type("closing_remark",$this->review->closing_remark)
                    ->type("complimentary_closing",$this->review->complimentary_closing)
                    ->type("project_number",$this->review->project_number)
                    ->press("Create Review")
                    ->assertPathIs("/review")
                    ->assertVisible('.alert-success')
                    ->assertSee("My Reviews");

            $this->assertDatabaseHas("reviews",[
                "project_number" => $this->review->project_number
            ]);

            $review = Review::first();


            $this->assertDatabaseHas("comment_review",[
                "review_id" => $review->id
            ]);

        });

    }

    /** @test */
    public function user_creates_a_review_with_failed_validation()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->user)
                    ->visit("/review")
                    ->clickLink("New Review")
                    ->press("Create Review")
                    ->assertPathIs("/review/create")
                    ->assertVisible('.error-msg');

        });
    }
}
