<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{

    use DatabaseMigrations;

    /** @test */
    public function a_user_can_login()
    {
        $user = factory(User::class)->create([
            "email" => "test@email.com",
            "password" => bcrypt("123456")
        ]);


        $this->browse(function (Browser $browser) use ($user) {

            $browser->visit('login')
                    ->type("email",$user->email)
                    ->type("password","123456")
                    ->press('LOG IN')
                    ->assertPathIs('/home');
        });
    }

    /** @test */
    public function user_inputs_invalid_credentials()
    {
        $this->browse(function (Browser $browser) {

            $browser->visit('login')
                    ->type("email","wrongemail@email.com")
                    ->type("password","wrongpassword")
                    ->press('LOG IN')
                    ->assertVisible('.error-msg');
        });

    }
}
