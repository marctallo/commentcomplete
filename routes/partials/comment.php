<?php

Route::middleware("auth")->group(function(){

    Route::post("comment/list","Comment\CommentController@getList")->name("comment.list");

    Route::resource('comment',"Comment\CommentController" , [
        "names" => [
            "index" => "comment.index"
        ]
    ]);

});
