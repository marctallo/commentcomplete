<?php

Route::middleware("auth")->group(function(){

    Route::resource("review" , "Review\ReviewController",[
          "names" => [
              "create"  =>  "review.create" ,
              "store"   =>  "review.store" ,
              "edit"    =>  "review.edit" ,
              "update"  =>  "review.update" ,
              "destroy" =>  "review.destroy"
          ]
    ]);

    Route::get("review/email/{review}","Review\EmailReviewController@sendEmail")->name("review.email");
});
