<?php

Route::middleware("auth")->group(function(){

  Route::get("account","User\UserAccountController@index")->name("account.index");

  Route::put("account/update/account/{user}","User\UserAccountController@updateAccount")->name("account.updateaccount");


});
