<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//set the partial file names

$route_partials = [
  "page",
  "user_account",
  "review",
  "comment",
  "download_review"
];


//loop through the partial list to include the files

foreach ($route_partials as $partial) {

    $file = __DIR__.'/partials/'.$partial.'.php';

    if ( ! file_exists($file))
    {
        $msg = "Route partial [{$partial}] not found.";

        throw new \Illuminate\Filesystem\FileNotFoundException($msg);
    }

    require_once $file;
}


Auth::routes();

Route::get("home","HomeController@index");

Route::get("test",function() {

    //\Mail::to("marctallo@gmail.com")->send(new \App\Mail\ReportMailer( \App\Review::first(), \App\User::first()));

    return view("test")->with([
        "user" => \App\User::first(),
        "review" => \App\Review::first()
    ]);
});
