<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentReview extends Model
{
    protected $table = "comment_review";
}
