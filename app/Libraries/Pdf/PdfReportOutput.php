<?php

namespace App\Libraries\Pdf;

use App\Contracts\ReportOutputDownloadInterface;

class PdfReportOutput implements ReportOutputDownloadInterface
{
    public function download($view , $data , $filename)
    {

        $pdf = \PDF::loadView($view, $data);

        return @$pdf->download($filename.".pdf");
    }
}
