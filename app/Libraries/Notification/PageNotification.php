<?php

namespace App\Libraries\Notification;

use App\Contracts\NotificationInterface;

class PageNotification implements NotificationInterface
{
    /**
     * Triggers the page notification by setting session variables.
     *
     * @param  $type = bootstrap 3 alert classes info|warning|success|danger
     * @param $message = custom message to display
     * @return void
     */
    public function show($type , $message)
    {
        session()->flash("show_notification",true);

        session()->flash("notification.type",$type);

        session()->flash("notification.message",$message);

    }
}
