<?php

namespace App\Libraries\Upload;

use App\Contracts\UploadInterface;

class ImageUploader implements UploadInterface
{

    public function handle($file , $user ,$uploader)
    {

        if ($file != null) {
            return $uploader->upload($file , $user);
        }

    }
}
