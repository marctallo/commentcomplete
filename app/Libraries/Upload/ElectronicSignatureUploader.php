<?php

namespace App\Libraries\Upload;
use Illuminate\Support\Facades\Storage;

class ElectronicSignatureUploader
{

    public function upload($file , $user)
    {
        if ($user->electronic_signature) {

            if (Storage::disk("uploads")->exists($user->electronic_signature)) {

                Storage::disk("uploads")->delete($user->electronic_signature);
            }

        }

        return $file->store("users/".$user->id."/user_signature","uploads");
    }
}
