<?php

namespace App\Libraries\Upload;
use Illuminate\Support\Facades\Storage;

class CompanyLogoUploader
{

    public function upload($file , $user)
    {
        if ($user->company_logo) {

            if (Storage::disk("uploads")->exists($user->company_logo)) {

                Storage::disk("uploads")->delete($user->company_logo);
            }

        }

        return $file->store("users/".$user->id."/company_logo","uploads");
    }
}
