<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Queue\InteractsWithQueue;
use App\Contracts\NotificationInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Events\MessageSending;

class ReportMailerNotification
{
    public $notification;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(NotificationInterface $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(MessageSending $event)
    {
        $this->notification->show("info","Your email was sent.");
    }
}
