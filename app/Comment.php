<?php

namespace App;

use App\User;
use App\Review;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";

    protected $fillable = [
        "comment",
        "code",
        "section",
        "user_id"
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function reviews()
    {

        return $this->belongsToMany(Review::class);
    }

    public static function create($comment , $user)
    {

        $self = new static;

        $existing_comment = $self->exists($comment);

        if ($existing_comment) {

            return $existing_comment;
        }

        return $user->comments()->create(["comment" => $comment]);
    }

    private function exists($comment)
    {

        return static::where("comment","like","%{$comment}%")->first();
    }

}
