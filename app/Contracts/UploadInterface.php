<?php

namespace App\Contracts;

use App\User;

interface UploadInterface
{
    /**
    *
    * @return filepath name
    *
    */
    public function handle($file ,$user , $uploader);
}
