<?php

namespace App\Contracts;

use App\User;
use App\Review;

interface ReviewInterface
{

    public function all(User $user);

    public function create($data , User $user);

    public function update($data , Review $review , User $user);
}
