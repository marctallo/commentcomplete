<?php

namespace App\Contracts;

interface ReportOutputDownloadInterface
{

    public function download($view , $data , $filename);
}
