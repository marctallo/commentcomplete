<?php

namespace App\Contracts;

interface NotificationInterface
{

    public function show($type , $message);

}
