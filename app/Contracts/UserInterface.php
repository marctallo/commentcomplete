<?php

namespace App\Contracts;

use App\User;

interface UserInterface
{

    public function create($data);

    public function update($data , User $user);

    public function delete($user);
}
