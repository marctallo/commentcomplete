<?php

namespace App\Mail;

use App\User;
use App\Review;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReportMailer extends Mailable
{
    use Queueable, SerializesModels;


    public $review;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Review $review , User $user)
    {
        $this->review = $review;

        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->user->completeName();

        $address = $this->user->email;

        $subject = $this->review->project_title." - Review";

        return $this->view('review.mail')
                    ->from($address , $name)
                    ->subject($subject);
    }
}
