<?php

namespace App;

use App\User;
use App\Comment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = "reviews";

    protected $fillable = [
        "client_email",
        "client_name",
        "client_company",
        "client_address",
        "project_title",
        "project_location",
        "project_iteration",
        "salutation",
        "general_info",
        "closing_remarks",
        "complimentary_closing",
        "project_number",
        "user_id"
    ];


    /**
    * Creates a default form values for the create review form
    *
    * @return App\Review
    *
    **/
    public static function formDefault()
    {

        $default = new static;

        $default->date = Carbon::now()->format("d F Y");
        $default->client_email = "Via Email: ";
        $default->client_name = "Client Name";
        $default->client_company = "Client Company";
        $default->client_address = "Client Address";
        $default->project_title = "Project Title";
        $default->project_location = "Project Location";
        $default->project_iteration = "Project Iteration";
        $default->salutation = "Dear Client:";
        $default->general_info = "General Info";
        //$default->comment = "Your Comment";
        $default->closing_remark = "Closing Remarks";
        $default->complimentary_closing = "Respectfully Submitted";
        $default->project_number = "Project Number:";

        return $default;
    }

    public function hasClientEmail()
    {

        return $this->client_email != null && $this->client_email != "";
    }

    public function user()
    {

        $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->belongsToMany(Comment::class)->withTimestamps();
    }

}
