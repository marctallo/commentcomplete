<?php

namespace App;

use App\Review;
use App\Comment;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'first_name',
        'last_name',
        'phone_number',
        'company',
        'company_address',
        'credentials',
        'company_logo',
        'electronic_signature'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function completeName()
    {

        return $this->first_name." ".$this->last_name;
    }

    public function setCredentialsAttribute($value)
    {

        $this->attributes['credentials'] = $value == "default" ? null : $value;

    }

    public function setPasswordAttribute($value)
    {

        $this->attributes['password'] = bcrypt($value);

    }

    public function reviews()
    {
        return $this->hasMany(Review::class)->with("comments");
    }

    public function comments()
    {

        return $this->hasMany(Comment::class);
    }

}
