<?php

namespace App\Http\Controllers\Download;

use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\ReportOutputDownloadInterface;

class DownloadReviewController extends Controller
{

    protected $pdf_output;

    /**
     * Constructor
     *
     * @param App\Contracts\ReportOutputDownloadInterface $pdf_output
     * @return void
     */
    public function __construct(ReportOutputDownloadInterface $pdf_output)
    {
        parent::__construct();

        $this->pdf_output = $pdf_output;
    }

   /**
    * Outputs a downloadable pdf file
    *
    * @param \App\Review $review
    * @return pdf file
    */
    public function getPdf(Review $review)
    {
        $view = "review.pdf";

        $data = [
            "review" => $review,
            "user" => $this->current_user
        ];

        $filename = $review->project_title."- Review";

        return $this->pdf_output->download($view , $data , $filename);

    }
}
