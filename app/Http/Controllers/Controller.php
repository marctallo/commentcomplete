<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $current_user;

    public function __construct()
    {
        //set the current logged user
        $this->middleware(function ($request, $next) {

           $this->current_user =  \Auth::user();

           return $next($request);
       });
    }
}
