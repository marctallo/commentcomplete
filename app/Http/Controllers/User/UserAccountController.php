<?php

namespace App\Http\Controllers\User;

use Auth;
use App\User;
use App\Credential;
use App\Contracts\UserInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserAccountRequest;
use App\Libraries\Upload\CompanyLogoUploader;

class UserAccountController extends Controller
{

    protected $user;

    public function __construct(UserInterface $user)
    {

        parent::__construct();

        $this->user = $user;
    }


    public function index()
    {

        //session()->put('is_active', "active");

        return view("account.index")->with([

            "credentials" => Credential::dropDown(),
            "user" => $this->current_user,
            "default_active" => $this->setDefaultActiveTab()
        ]);
    }

    public function updateAccount(UserAccountRequest $request , User $user)
    {

        $this->user->update($request->all() , $user);

        return redirect("account");
    }

    private function setDefaultActiveTab()
    {

        if (session("password.is_active") == null && session("personal.is_active") == null &&session("company.is_active") == null) {

            return "active";
        }
    }

}
