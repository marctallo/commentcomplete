<?php

namespace App\Http\Controllers\Review;

use App\Review;
use Illuminate\Http\Request;
use App\Contracts\ReviewInterface;
use App\Http\Requests\ReviewRequest;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    protected $review;

    /**
     * ReviewController Contructor
     *
     * @param \App\Contracts\ReviewInterface $review
     * @return void
     */
    public function __construct(ReviewInterface $review)
    {

        parent::__construct();

        $this->review = $review;

    }

    /**
     * Shows the review list
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $reviews = $this->review->all($this->current_user);

        return view('review.index')->with([

            "reviews" => $reviews
        ]);
    }

    /**
     * Shows New Review Form
     *
     * @param \App\Review $review
     * @return \Illuminate\Http\Response
     */
    public function create(Review $review)
    {

        $default_review = Review::formDefault();

        return view("review.create")->with([

            "user" => $this->current_user,
            "default_review" => $default_review
        ]);
    }

    /**
     * Store a new review.
     *
     * @param  \App\Http\Requests\ReviewRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReviewRequest $request)
    {

        $this->review->create($request->all(),$this->current_user);

        return redirect("review");
    }

    /**
     * Display the specified review.
     *
     * @param  \App\Review $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {

        //dd($review->comments);
        $this->authorize('view', $review);

        return view("review.show")->with([

            "review" => $review,
            "user" => $this->current_user
        ]);
    }

    /**
     * Show the form for editing the review.
     *
     * @param  \App\Review $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        $this->authorize('view', $review);

        return view("review.edit")->with([

            "review" => $review,
            "user" => $this->current_user
        ]);
    }

    /**
     * Update the specified review.
     *
     * @param  \App\Http\Requests\ReviewRequest  $request
     * @param  \App\Review $review
     * @return \Illuminate\Http\Response
     */
    public function update(ReviewRequest $request, Review $review)
    {
        $this->authorize('update', $review);

        $this->review->update($request->all(), $review ,$this->current_user);

        return redirect("review");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        $this->authorize('delete', $review);

        $this->review->delete($review);

        return redirect("review");
    }
}
