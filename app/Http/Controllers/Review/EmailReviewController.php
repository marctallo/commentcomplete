<?php

namespace App\Http\Controllers\Review;

use App\Review;
use App\Mail\ReportMailer;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Mail\Mailer;

class EmailReviewController extends Controller
{

    protected $mail;

    /**
     * Constructor
     *
     * @param \Illuminate\Support\Facades\Mail
     *
     * @return void
     */
    public function __construct(Mailer $mail)
    {
        parent::__construct();

        $this->mail = $mail;
    }

    /**
     * Constructor
     *
     * @param App\Mail\ReportMailer
     *
     * @return void
     */
    public function sendEmail(Review $review)
    {

        if ($review->hasClientEmail()) {

            $this->mail->to($review->client_email)->send(new ReportMailer($review , $this->current_user));
        }

        return redirect("review");

    }
}
