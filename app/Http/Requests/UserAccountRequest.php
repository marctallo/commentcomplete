<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        switch($this->request->get("submit_button")) {

            case "account" :

                session()->flash('account.is_active', 'active');

                $rules = [
                    'username' => 'required|string|max:255|unique:users,username,'.$this->user->id,
                    'email' => 'required|string|email|max:255|unique:users,email,'.$this->user->id,
                ];

            break;

            case "password" :

                session()->flash('password.is_active', 'active');

                $rules = [
                    'old_password' => 'required|old_password:' . \Auth::user()->password,
                    'password' => 'required|string|min:6|confirmed',
                ];

            break;

            case "personal" :

                session()->flash('personal.is_active', 'active');

                $rules = [
                    'electronic_signature' => 'sometimes|nullable|mimes:jpeg,gif,png'
                ];

            break;

            case "company" :

                session()->flash('company.is_active', 'active');

                $rules = [
                    'company_logo' => 'sometimes|nullable|mimes:jpeg,gif,png',
                ];

            break;

            default :

        }


        return $rules;
    }
}
