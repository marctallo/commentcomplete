<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'client_email' => 'sometimes|nullable|email',
            'client_name' => 'required|string|max:255',
            'client_company' => 'required|string|max:255',
            'client_address' => 'required|string',
            'project_title' => 'required|string|max:255',
            'project_location' => 'required|string',
            'project_iteration' => 'required|string|max:255',
            'project_number' => 'required|string|max:255',
            'salutation' => 'required|string|max:255',
            'general_info' => 'required|string',
            'complimentary_closing' => 'required|string|max:255'
        ];


        return $rules;
    }


}
