<?php

namespace App\Providers;

use App\User;
use App\Review;
use App\Observers\UserObserver;
use App\Observers\ReviewObserver;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        User::observe(UserObserver::class);

        Review::observe(ReviewObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
