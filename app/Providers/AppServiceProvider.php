<?php

namespace App\Providers;

use Illuminate\Support\Facades\Hash;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //fix for string length error on migration
        Schema::defaultStringLength(191);

        //dusk setup
        if ($this->app->environment('local', 'testing')) {

            $this->app->register(DuskServiceProvider::class);
        }

        //custom old password validation rule
        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {

            return Hash::check($value, current($parameters));

        });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //Image uploader for company logo and electronic signature
        $this->app->bind('App\Contracts\UploadInterface','App\Libraries\Upload\ImageUploader');

        //Page notification shows when events like add , edit or delete occur
        $this->app->bind("App\Contracts\NotificationInterface","App\Libraries\Notification\PageNotification");

        //Pdf output downloader
        $this->app->bind("App\Contracts\ReportOutputDownloadInterface","App\Libraries\Pdf\PdfReportOutput");
    }
}
