<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credential extends Model
{
    public $timestamps = true;

    protected $fillabe = ["name"];

    public static function dropDown()
    {

        $credentials = static::latest()->get()->pluck("name","name");

        $credentials = $credentials->all();

        return array_prepend($credentials ,"Select Credentials","default");
    }
}
