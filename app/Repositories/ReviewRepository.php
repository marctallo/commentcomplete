<?php

namespace App\Repositories;

use DB;
use App\User;
use App\Review;
use App\Comment;
use App\CommentReview;
use App\Contracts\ReviewInterface;

class ReviewRepository implements ReviewInterface
{

    protected $review;

    protected $comment;

    /**
     * Constructor
     *
     * @param \App\Review $review
     * @param \App\Comment $comment
     *
     * @return void
     */
    public function __construct(Review $review , Comment $comment)
    {

        $this->review = $review;

        $this->comment = $comment;
    }

    /**
     * Retrieves all reviews associated to user
     *
     * @param \App\User $user
     *
     * @return \App\User
     */
    public function all(User $user)
    {
        return $user->reviews()
                    ->orderBy("updated_at","desc")
                    ->get();
    }

    /**
     * Creates a review and add comment
     *
     * @param Array $data
     * @param \App\User $user
     *
     * @return \App\Review
     */
    public function create($data , User $user)
    {

        return DB::transaction(function () use ($data , $user){

            $data["user_id"] = $user->id;

            $review = $this->createReview($data);

            $this->attachComment($data , $review , $user);

            return $review;

        });

    }

    /**
     * Updates review and its comments
     *
     * @param Array $data
     * @param \App\Review $review
     * @param \App\User $user
     *
     * @return \App\Review
     */
    public function update($data , Review $review , User $user)
    {

        return DB::transaction(function () use ($data , $review ,$user){

            $data["user_id"] = $user->id;

            $review = $this->updateReview($review,$data);

            $this->detachComment($review);

            $this->attachComment($data , $review , $user);

            return $review;

        });
    }

    /**
     * Deletes a user review
     *
     * @param \App\Review $review
     *
     * @return Boolean
     */
    public function delete(Review $review)
    {

        $current_review = $this->review
                    ->where("id","=",$review->id)
                    ->first();

        if ($current_review) {

            return $current_review->delete();
        }


        return false;

    }

    /**
     * Add comments to the pivot table
     *
     * @param Array $data
     * @param \App\Review $review
     * @param \App\User $user
     *
     * @return void
     */
    private function attachComment($data , Review $review , User $user)
    {
        if (isset($data["comment"]) && $data["comment"] != null) {

            foreach ($data["comment"] as $comment_value) {

                if ($comment_value != null) {

                    $comment = $this->comment->create($comment_value ,$user);

                    $review->comments()->attach($comment->id, ["comment" => $comment_value]);

                }

            }
        }
    }

    /**
     * Removes comments in the pivot table
     *
     * @param \App\Review $review
     *
     * @return void
     */
    private function detachComment(Review $review)
    {
        $comment_ids = $review->comments()->pluck("id");

        if (count($comment_ids->all()) > 0) {

            $review->comments()->detach($comment_ids->all());
        }



    }

    /**
     * Private wrapper to create a review
     *
     * @param Array $data
     *
     * @return \App\Review
     */
    private function createReview($data)
    {

        return $this->review->create($data);
    }

    /**
     * Private wrapper to update a review
     *
     * @param \App\Review $review
     * @param Array $data
     *
     * @return \App\Review
     */
    private function updateReview(Review $review , $data)
    {
        $review->update($data);

        return $review;
    }

}
