<?php

namespace App\Repositories;

use App\User;
use App\Contracts\UserInterface;
use App\Contracts\UploadInterface;
use App\Libraries\Upload\CompanyLogoUploader;
use App\Libraries\Upload\ElectronicSignatureUploader;

class UserRepository implements UserInterface
{

    protected $uploader;

    /**
     * Constructor
     *
     * @param \App\Contracts\UploadInterface $uploader
     *
     * @return void
     */
    public function __construct(UploadInterface $uploader)
    {

        $this->uploader = $uploader;
    }

    /**
     * Creates a new user
     *
     * @param Array $data
     *
     * @return \App\User;
     */
    public function create($data)
    {

        $user = User::create($data);

        return $user;
    }

    /**
     * Updates user
     *
     * @param Array $data
     * @param \App\User $user
     *
     * @return \App\User
     */
    public function update($data , User $user)
    {
        
        if (array_key_exists("company_logo" , $data)) {

            $filename = $this->uploader->handle($data["company_logo"] , $user , new CompanyLogoUploader);

            $data["company_logo"] = $filename;
        }

        if (array_key_exists("electronic_signature" , $data)) {

            $filename = $this->uploader->handle($data["electronic_signature"] ,$user, new ElectronicSignatureUploader);

            $data["electronic_signature"] = $filename;
        }


        $user->update($data);

        return $user;

    }

    public function delete($user)
    {


    }

}
