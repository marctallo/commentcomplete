<?php

namespace App\Observers;

use App\Review;

use App\Contracts\NotificationInterface;

class ReviewObserver
{

    protected $notification;

    public function __construct(NotificationInterface $notification)
    {
        $this->notification = $notification;
    }

    public function created(Review $review)
    {
        $this->notification->show("success","A new review has been created.");
    }


    public function updated(Review $review)
    {

        $this->notification->show("success","Review has been updated.");
    }

    public function deleted(Review $review)
    {
        $this->notification->show("danger","You have successfully deleted a review.");
    }
}
