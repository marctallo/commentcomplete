<?php

namespace App\Observers;

use App\User;

use App\Contracts\NotificationInterface;

class UserObserver
{

    protected $notification;

    public function __construct(NotificationInterface $notification)
    {
        $this->notification = $notification;
    }

    public function updated(User $user)
    {

        $this->notification->show("info","Account successfully updated.");
    }
}
