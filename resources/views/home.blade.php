@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            @include('page.partials.notification_container')
            <div class="content-container">
                <h2>My Reviews </h2>
                <hr>
                <a href="{{ route('review.create') }}" class="btn btn-primary pull-right new-review-btn">New Review</a>
                <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th>Project Name</th>
                          <th>Review Iteration</th>
                          <th>Client</th>
                          <th>Date Created</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>Test Project</td>
                          <td>Iteration 1</td>
                          <td>Client 1</td>
                          <td>May 02, 2017</td>
                      </tr>
                      <tr>
                          <td>Test Project</td>
                          <td>Iteration 1</td>
                          <td>Client 1</td>
                          <td>May 02, 2017</td>
                      </tr>
                      <tr>
                          <td>Test Project</td>
                          <td>Iteration 1</td>
                          <td>Client 1</td>
                          <td>May 02, 2017</td>
                      </tr>
                      <tr>
                          <td>Test Project</td>
                          <td>Iteration 1</td>
                          <td>Client 1</td>
                          <td>May 02, 2017</td>
                      </tr>
                      <tr>
                          <td>Test Project</td>
                          <td>Iteration 1</td>
                          <td>Client 1</td>
                          <td>May 02, 2017</td>
                      </tr>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
