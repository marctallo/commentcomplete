<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>{{ $review->project_title }}</title>
    @yield("email.style")
    <!--The MIT License (MIT).

    Copyright (c) 2015 Ted Goas

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.-->

    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->

    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
        <style>
            * {
                font-family: sans-serif !important;
            }
        </style>
    <![endif]-->

    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <!--[if !mso]><!-->
        <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
    <!--<![endif]-->

    <!-- Web Font / @font-face : END -->

    <!-- CSS Reset -->
    <style>

        html,
        body {
            font-weight: 400 !important;
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }

        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }

        img {
            -ms-interpolation-mode:bicubic;
        }

        *[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
        }

        .x-gmail-data-detectors,
        .x-gmail-data-detectors *,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
        }

        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        img.g-img + div {
            display:none !important;
           }

        .button-link {
            text-decoration: none !important;
        }

        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
            .email-container {
                min-width: 375px !important;
            }
        }
    </style>

    <style>

        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }
        /* Media Queries */
        @media screen and (max-width: 480px) {

            .fluid {
                width: 100% !important;
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }

            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }

            .stack-column-center {
                text-align: center !important;
            }

            .company-logo {

                display: none;
                text-align: left !important;
            }

            .top-spacer {

                padding: 40px 0 0 !important;
            }
            .left-column , .right-column {

                float: none !important;
                text-align: left !important;
                width: 100% !important;

            }

            .comment-number {

                padding: 0 0 0 50px !important;
            }

            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }

			.email-container p {
				font-size: 16px !important;
				line-height: 22px !important;
			}

            .email-container p.copyright {

                font-size: 12px !important;
            }
        }
    </style>

    <!--[if gte mso 9]>
    <xml>
      <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
     </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->

</head>
<body width="100%" bgcolor="#222222" style="margin: 0; mso-line-height-rule: exactly;">
    <center style="width: 100%; background: #222222; text-align: left;">
        <div style="max-width: 680px; margin: auto;" class="email-container">
            <!--[if mso]>
            <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="680" align="center">
            <tr>
            <td>
            <![endif]-->
            <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;" class="email-container">


                <tr class="email-report-heading">
                    <td bgcolor="#ffffff">
                        <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td class="top-spacer" style="padding: 20px 0 0"></td>
                            </tr>
                            @if($user->company_logo)
                                <tr>
                                    <td class="company-logo" style="padding: 0 40px 10px; text-align: right;">
                                        <img src="{{ asset('uploads/'.$user->company_logo)}}" alt="Company Logo"  aria-hidden="true" width="200" height="100" border="0" style="height: 100px; width:200px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555;">
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td style="padding: 0 40px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <p style="margin: 0;">{{  $review->created_at->format("d F Y") }}</p>
                                </td>
                            </tr>
                            @if($review->client_email && $review->client_email != "")
                                <tr>
                                    <td style="padding: 0 40px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                        <p style="margin: 0;">Via Email: email@gmail.com</p>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td style="padding: 0 40px 10px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <p style="margin: 0;">{{ $review->client_name}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0 40px 10px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <p style="margin: 0;">{{ $review->client_company }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0 40px 10px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <?php $address = explode("<br />",nl2br($review->client_address)); ?>
                                    @foreach($address as $value)
                                        @if($value != "")
                                            <p style="margin: 0; padding:0 0 10px;">{{ $value }}</p>
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0 40px 10px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <p style="margin: 0; font-weight:700; text-transform: uppercase;">{{ $review->project_title }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0 40px 10px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <p style="margin: 0; font-weight:700; text-transform: uppercase;">{{ $review->project_location }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0 40px 30px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <p style="margin: 0; font-weight:700; text-transform: uppercase;">{{ $review->project_iteration }}</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="email-report-body">
                    <td bgcolor="#ffffff">
                        <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="padding: 0 40px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <p style="margin: 0;">{{ $review->salutation }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0 40px 10px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <?php $general_info = explode("<br />",nl2br($review->general_info)); ?>
                                    @foreach($general_info as $value)
                                        @if($value != "")
                                            <p style="margin: 0; padding:0 0 10px">{{ $value }}</p>
                                        @endif
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" class="comment-container" align="left">
                                        @if(count($review->comments) > 0)
                                            @foreach($review->comments as $count => $comment)
                                        <tr>
                                            <td style="padding: 0 0 0 60px;font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;" valign="top" class="comment-number">
                                                {{ $count+1 }}.
                                            </td>
                                            <td style="padding: 0 40px 0 0; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                                <p style="margin: 0; padding:0 0 8px 10px">{{ $comment->comment }} </p>
                                            </td>
                                        </tr>
                                            @endforeach
                                        @endif
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 40px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <p style="margin: 0;">{{ $review->closing_remarks }}</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="email-report-footer">
                    <td bgcolor="#ffffff">
                        <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="padding: 0 40px 20px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <p style="margin: 0;">{{ $review->complimentary_closing }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0 40px 10px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <p style="margin: 0; font-weight:700; text-transform:uppercase">{{ $user->company }}</p>
                                </td>
                            </tr>
                            @if($user->electronic_signature)
                                <tr>
                                    <td style="padding: 0 40px 10px; text-align: left;">
                                        <img src="{{ asset('uploads/'.$user->electronic_signature)}}" alt="Electronic Signature"  aria-hidden="true" width="200" height="100" border="0" style="height: 100px; width:200px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555;">
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td style="padding: 0 40px 10px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <p style="margin: 0;">{{ $user->first_name." ".$user->last_name }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0 40px 40px; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555; text-align: left;">
                                    <p style="margin: 0;">{{ $user->credentials }}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td>
                                                <div class="right-column" style="width:50%; float:right;  text-align: right;">
                                                    <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                        <tr>
                                                            <td style="padding: 0 40px 0; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555;">
                                                                <p style="margin: 0; padding:0 0 8px 0">{{ $user->phone_number }}</p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="left-column" style="width:50%; float:left; text-align: left;">
                                                    <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                        <tr>
                                                            <td style="padding: 0 40px 0; font-family: sans-serif; font-size: 14px; line-height: 20px; color: #555555;">
                                                                <p style="margin: 0; padding:0 0 8px 0">{{ $review->project_number }}</p>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                @yield("email.footer")
            </table>

            <!--[if mso]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </div>
    </center>
    @yield("email.script")
</body>
</html>
