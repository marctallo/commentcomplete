@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                @include('page.partials.notification_container')
                <div class="content-container">
                    <div id="account-form-container">
                        <h2>Account</h2>
                        <hr>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="{{ $default_active }}"><a href="#account-tab-account" aria-controls="account-tab-account" role="tab" data-toggle="tab">Account</a></li>
                            <li role="presentation" class="{{ session('password.is_active') }}"><a href="#account-tab-password" aria-controls="account-tab-password" role="tab" data-toggle="tab">Password</a></li>
                            <li role="presentation" class="{{ session('personal.is_active') }}"><a href="#account-tab-personal" aria-controls="account-tab-personal" role="tab" data-toggle="tab">Personal</a></li>
                            <li role="presentation" class="{{ session('company.is_active') }}"><a href="#account-tab-company" aria-controls="account-tab-company" role="tab" data-toggle="tab">Company</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane {{ $default_active }}" id="account-tab-account">
                                {!! Form::model($user,['method'=>'put','route'=>['account.updateaccount',$user]]) !!}
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    {!! Form::text('username',null,['class'=>'form-control register-field','required']) !!}
                                    @if ($errors->has('username'))
                                        <p class="error-msg">{{ $errors->first('username') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="email">Email Address</label>
                                    {!! Form::text('email',null,['class'=>'form-control register-field','required']) !!}
                                    @if ($errors->has('email'))
                                        <p class="error-msg">{{ $errors->first('email') }}</p>
                                    @endif
                                </div>
                                <button class="btn btn-primary" type="submit" name="submit_button" value="account">Update</button>
                                {!! Form::close() !!}
                            </div>
                            <div role="tabpanel" class="tab-pane {{ session('password.is_active') }}" id="account-tab-password">
                                {!! Form::model($user,['method'=>'put','route'=>['account.updateaccount',$user]]) !!}
                                <div class="form-group">
                                    <label for="old_password">Old Password</label>
                                    {!! Form::password('old_password',['class'=>'form-control register-field','required']) !!}
                                    @if ($errors->has('old_password'))
                                        <p class="error-msg">{{ $errors->first('old_password') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    {!! Form::password('password',['class'=>'form-control register-field','required']) !!}
                                    @if ($errors->has('password'))
                                        <p class="error-msg">{{ $errors->first('password') }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">Confirm Password</label>
                                    {!! Form::password('password_confirmation',['class'=>'form-control register-field','required']) !!}
                                </div>
                                <button class="btn btn-primary" type="submit" name="submit_button" value="password">Change Password</button>
                                {!! Form::close() !!}
                            </div>
                            <div role="tabpanel" class="tab-pane {{ session('personal.is_active') }}" id="account-tab-personal">
                                {!! Form::model($user,['method'=>'put','route'=>['account.updateaccount',$user],'files'=> true]) !!}
                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    {!! Form::text('first_name',null,['class'=>'form-control register-field']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    {!! Form::text('last_name',null,['class'=>'form-control register-field']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="phone_number">Phone Number</label>
                                    {!! Form::text('phone_number',null,['class'=>'form-control register-field']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="electronic_signature">Electronic Signature <small>( jpg,gif & png only. must be 200x100 )</small></label>
                                    <div class="electronic-signature-wrapper">
                                        @if($user->electronic_signature)
                                            <img id="electronic-signature" src="{{ asset('uploads/'.$user->electronic_signature) }}" alt="Electronic Signature">
                                        @endif
                                        <div class="file-upload-container btn btn-default">
                                            <span class="file-upload-text">Upload Signature</span>
                                            {!! Form::file('electronic_signature',['class'=>'form-control register-field upload file-upload-btn']) !!}
                                        </div>
                                        @if ($errors->has('electronic_signature'))
                                            <p class="error-msg">{{ $errors->first('electronic_signature') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <button class="btn btn-primary" type="submit" name="submit_button" value="personal">Update</button>
                                {!! Form::close() !!}
                            </div>
                            <div role="tabpanel" class="tab-pane {{ session('company.is_active') }}" id="account-tab-company">
                                {!! Form::model($user,['method'=>'put','route'=>['account.updateaccount',$user],'files'=> true]) !!}
                                <div class="form-group">
                                    <label for="company">Company</label>
                                    {!! Form::text('company',null,['class'=>'form-control register-field']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="company_address">Company Address</label>
                                    {!! Form::text('company_address',null,['class'=>'form-control register-field']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="credentials">Credentials</label>
                                    {!! Form::select('credentials', $credentials,null,['class'=>'form-control register-field']); !!}
                                </div>
                                <div class="form-group">
                                    <label for="company_logo">Company Logo <small>( jpg,gif & png only. must be 200x100 )</small></label>
                                    <div class="company-logo-wrapper">
                                        @if($user->company_logo)
                                            <img id="company-logo" src="{{ asset('uploads/'.$user->company_logo) }}" alt="Company Logo">
                                        @endif
                                        <div class="file-upload-container btn btn-default">
                                            <span class="file-upload-text">Upload Logo</span>
                                            {!! Form::file('company_logo',['class'=>'form-control register-field upload file-upload-btn']) !!}
                                        </div>

                                        @if ($errors->has('company_logo'))
                                            <p class="error-msg">{{ $errors->first('company_logo') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <button class="btn btn-primary" type="submit" name="submit_button" value="company">Update</button>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
