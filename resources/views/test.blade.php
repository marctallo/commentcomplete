<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
    .fileUpload {
        position: relative;
        overflow: hidden;
        margin: 10px;
        }
        .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
        }

    </style>
    <body>
        <div class="fileUpload btn btn-primary">
            <span>Upload</span>
            <input type="file" class="upload" />
        </div>
    </body>
    <script type="text/javascript">
            document.getElementById("uploadBtn").onchange = function () {
        document.getElementById("uploadFile").value = this.value;
        };
    </script>
</html>
