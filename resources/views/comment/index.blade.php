@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            @include('page.partials.notification_container')
            <div class="content-container comments-list-container">
                <h2>Comments</h2>
                <hr>
                @if(count($comments) > 0)

                    @foreach($comments as $comment)
                    <div class="panel panel-default">
                        <div class="panel-heading">{{ $comment->user->company }} - {{ $comment->user->username }}  <i class="fa fa-thumbs-o-up pull-right"></i> <i class="fa fa-thumbs-o-down pull-right"></i><i class="fa fa-heart-o pull-right"></i></div>
                        <div class="panel-body">{{ $comment->comment }}</div>
                    </div>
                    @endforeach

                @endif
            </div>
        </div>
    </div>
</div>
@endsection
