@include('page.partials.header')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div id="login-wrapper">
                <div id="login-container">
                    <h1>Login to Comment Complete</h1>
                    {!! Form::open() !!}
                    <div class="form-group">
                        <label for="email">Email Address</label>
                        {!! Form::text('email',null,['class'=>'form-control login-field','required']) !!}
                        @if ($errors->has('email'))
                            <p class="error-msg">{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="email">Password</label>
                        {!! Form::password('password',['class'=>'form-control login-field','required']) !!}
                    </div>
                    <a href="{{ route('password.request') }}" class="forgot-password-btn">forgot password?</a>
                    <button class="btn btn-primary login-btn" type="submit">log in</button>
                    <p><small>Don't have an account? <a href="{{ route('register') }}">Sign up</a></small></p>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
</div>
@include('page.partials.footer')
