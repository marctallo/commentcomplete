@include('page.partials.header')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div id="forget-password-wrapper">
                @if (session('status'))
                    <div class="alert alert-success status-box">
                        {{ session('status') }}
                    </div>
                @endif
                <div id="forget-password-container">
                    <h1>Password Reset</h1>
                    {!! Form::open(['route'=>'password.request','method'=>'post']) !!}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="form-group">
                        <label for="email">Email Address</label>
                        {!! Form::text('email',null,['class'=>'form-control forget-password-field','required']) !!}
                        @if ($errors->has('email'))
                            <p class="error-msg">{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        {!! Form::password('password',['class'=>'form-control forget-password-field','required']) !!}
                        @if ($errors->has('password'))
                            <p class="error-msg">{{ $errors->first('password') }}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Confirm Password</label>
                        {!! Form::password('password_confirmation',['class'=>'form-control forget-password-field','required']) !!}
                        @if ($errors->has('password_confirmation'))
                            <p class="error-msg">{{ $errors->first('password_confirmation') }}</p>
                        @endif
                    </div>
                    <button class="btn btn-primary login-btn" type="submit">reset password</button>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
</div>
@include('page.partials.footer')
