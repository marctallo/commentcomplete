@include('page.partials.header')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div id="register-wrapper">
                <div id="register-container">
                    <h1>Register to Comment Complete</h1>
                    {!! Form::open(['route'=>'register','method'=>'post','files' => true]) !!}
                    <fieldset>
                        <legend>Account Info</legend>
                        <div class="form-group">
                            <label for="username">Username <small>(required)</small></label>
                            {!! Form::text('username',null,['class'=>'form-control register-field','required']) !!}
                            @if ($errors->has('username'))
                                <p class="error-msg">{{ $errors->first('username') }}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">Email Address <small>(required)</small></label>
                            {!! Form::text('email',null,['class'=>'form-control register-field','required']) !!}
                            @if ($errors->has('email'))
                                <p class="error-msg">{{ $errors->first('email') }}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password">Password <small>(required , must be at least 6 characters)</small></label>
                            {!! Form::password('password',['class'=>'form-control register-field','required']) !!}
                            @if ($errors->has('password'))
                                <p class="error-msg">{{ $errors->first('password') }}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Confirm Password (required)</label>
                            {!! Form::password('password_confirmation',['class'=>'form-control register-field','required']) !!}
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Personal Info</legend>

                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            {!! Form::text('first_name',null,['class'=>'form-control register-field']) !!}
                        </div>
                        <div class="form-group">
                            <label for="last_name">Last Name</label>
                            {!! Form::text('last_name',null,['class'=>'form-control register-field']) !!}
                        </div>
                        <div class="form-group">
                            <label for="phone_number">Phone Number</label>
                            {!! Form::text('phone_number',null,['class'=>'form-control register-field']) !!}
                        </div>

                    </fieldset>
                    <fieldset>
                        <legend>Company Info</legend>
                        <div class="form-group">
                            <label for="company">Company</label>
                            {!! Form::text('company',null,['class'=>'form-control register-field']) !!}
                        </div>
                        <div class="form-group">
                            <label for="company_address">Company Address</label>
                            {!! Form::text('company_address',null,['class'=>'form-control register-field']) !!}
                        </div>
                        <div class="form-group">
                            <label for="credentials">Credentials</label>
                            {!! Form::select('credentials', $credentials,null,['class'=>'form-control register-field']); !!}
                        </div>
                        <div class="form-group">
                            <label for="company_logo">Company Logo (png , jpg , gif only)</label>
                            {!! Form::file('company_logo',['class'=>'form-control register-field']) !!}
                            @if ($errors->has('company_logo'))
                                <p class="error-msg">{{ $errors->first('company_logo') }}</p>
                            @endif
                        </div>

                    </fieldset>

                    <button class="btn btn-primary login-btn" type="submit">Register</button>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
</div>
@include('page.partials.footer')
