@include('page.partials.header')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div id="register-wrapper">
                <div id="register-container">
                    <h1>Register to Comment Complete</h1>
                    {!! Form::open(['route'=>'register','method'=>'post','files' => true]) !!}
                        <div class="form-group">
                            <label for="username">Username <small>(required)</small></label>
                            {!! Form::text('username',null,['class'=>'form-control register-field','required']) !!}
                            @if ($errors->has('username'))
                                <p class="error-msg">{{ $errors->first('username') }}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email">Email Address <small>(required)</small></label>
                            {!! Form::text('email',null,['class'=>'form-control register-field','required']) !!}
                            @if ($errors->has('email'))
                                <p class="error-msg">{{ $errors->first('email') }}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password">Password <small>(required , must be at least 6 characters)</small></label>
                            {!! Form::password('password',['class'=>'form-control register-field','required']) !!}
                            @if ($errors->has('password'))
                                <p class="error-msg">{{ $errors->first('password') }}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Confirm Password <small>(required)</small></label>
                            {!! Form::password('password_confirmation',['class'=>'form-control register-field','required']) !!}
                        </div>
                    <button class="btn btn-primary login-btn" type="submit">Register</button>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
</div>
@include('page.partials.footer')
