@if(session('show_notification'))
    <div class="notification-container">
        <div class="alert alert-{{session('notification.type') }} alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Notification:</strong> {{session('notification.message') }}
        </div>
    </div>
@endif
