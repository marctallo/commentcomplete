@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="content-container">
                    <div class="review-form-container">

                        {!! Form::model($review,['method'=>'put','route'=>['review.update',$review]]) !!}
                        <div class="review-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    @if($user->company_logo)
                                        <img class="company-logo pull-right" src="{{ asset('uploads/'.$user->company_logo)}}" alt="Company Logo">
                                    @endif
                                </div>
                            </div>
                            <div class="row inside-address">
                                <div class="col-xs-12">
                                    <p class="date-created">{{  $review->date }}</p>
                                    <div class="form-group">
                                        {!! Form::email("client_email",null,["class" => "review-input client-email" , "placeholder" => "Client Email"]) !!}
                                        @if ($errors->has('client_email'))
                                            <p class="error-msg">{{ $errors->first('client_email') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text("client_name",old('client_name', $review->client_name),["class" => "review-input client-name" , "placeholder" => "Client Name" , "required" => "required"]) !!}
                                        @if ($errors->has('client_name'))
                                            <p class="error-msg">{{ $errors->first('client_name') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text("client_company",old('client_company', $review->client_company),["class" => "review-input client-company" , "placeholder" => "Client Company" , "required" => "required"]) !!}
                                        @if ($errors->has('client_company'))
                                            <p class="error-msg">{{ $errors->first('client_company') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::textarea("client_address",old('client_address', $review->client_address),["class" => "review-input client-address review-textarea" , "rows" => 1 , "placeholder" => "Client Address" , "required" => "required"]) !!}
                                        @if ($errors->has('client_address'))
                                            <p class="error-msg">{{ $errors->first('client_address') }}</p>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="review-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="project-details">
                                        <div class="form-group">
                                            {!! Form::text("project_title",old('project_title', $review->project_title),["class" => "review-input project-title" , "placeholder" => "Project Title" , "required" => "required"]) !!}
                                            @if ($errors->has('project_title'))
                                                <p class="error-msg">{{ $errors->first('project_title') }}</p>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            {!! Form::text("project_location",old('project_location', $review->project_location),["class" => "review-input project-location" , "placeholder" => "Project Location" , "required" => "required"]) !!}
                                            @if ($errors->has('project_location'))
                                                <p class="error-msg">{{ $errors->first('project_location') }}</p>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            {!! Form::text("project_iteration",old('project_iteration', $review->project_iteration),["class" => "review-input project-iteration" , "placeholder" => "Project Iteration" , "required" => "required"]) !!}
                                            @if ($errors->has('project_iteration'))
                                                <p class="error-msg">{{ $errors->first('project_iteration') }}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text("salutation",old('salutation', $review->salutation),["class" => "review-input salutation" , "placeholder" => "Salutation" , "required" => "required"]) !!}
                                        @if ($errors->has('salutation'))
                                            <p class="error-msg">{{ $errors->first('salutation') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::textarea("general_info",old('general_info', $review->general_info),["class" => "review-input general-info review-textarea" ,"rows" => 3 , "placeholder" => "General Info" , "required" => "required"]) !!}
                                        @if ($errors->has('general_info'))
                                            <p class="error-msg">{{ $errors->first('general_info') }}</p>
                                        @endif
                                    </div>
                                    <div class="comment-container">
                                        {{ csrf_field() }}
                                        <ol class="comment-list">

                                            @foreach(old("comment",$review->comments()->pluck("comment_review.comment")) as $comments)

                                                <li>
                                                    <textarea name="comment[]" class="review-input comment-item" rows="1" required="required">{{ $comments }}</textarea> <i class="fa fa-remove remove-comment-btn"></i>
                                                </li>
                                            @endforeach

                                        </ol>
                                        <a href="#" class="add-comment-btn">add comment</a>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::textarea("closing_remarks",old('closing_remark', $review->closing_remark),["class" => "review-input closing-remark review-textarea" ,"rows" => 1 ,"placeholder" => "Closing Remarks"]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="review-footer">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        {!! Form::text("complimentary_closing",old('complimentary_closing', $review->complimentary_closing),["class" => "review-input complimentary-closing" , "placeholder" => "Closing Remarks" , "required" => "required"]) !!}
                                        @if ($errors->has('complimentary_closing'))
                                            <p class="error-msg">{{ $errors->first('complimentary_closing') }}</p>
                                        @endif
                                    </div>
                                    <p class="company-name">{{ $user->company }}</p>
                                    @if($user->electronic_signature)
                                        <img class="electronic-signature" src="{{ asset('uploads/'.$user->electronic_signature)}}" alt="Electronic Signature">
                                    @endif
                                    <p class="user-name">{{ $user->first_name." ".$user->last_name }}</p>
                                    <p class="user-title">{{ $user->credentials }}</p>
                                </div>
                            </div>
                            <div class="row project-footer">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        {!! Form::text("project_number",old('project_number', $review->project_number),["class" => "review-input project-number" , "placeholder" => "Project Number" ,"required" => "required"]) !!}
                                        @if ($errors->has('project_number'))
                                            <p class="error-msg">{{ $errors->first('project_number') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <p class="user-phonenumber pull-right">{{ $user->phone_number }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="copyright">©{{ Date("Y") }} {{ $user->company }}  All Rights Reserved</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <button class="btn btn-primary create-review-btn">Update</button>
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/template" id="comment-container-tpl">
        <li>
            <textarea class="review-input comment-item" rows="1"></textarea> <i class="fa fa-remove remove-comment-btn"></i>
        </li>
    </script>
@endsection
