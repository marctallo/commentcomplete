@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="content-container">
                    <div class="review-form-container">

                        {!! Form::model($default_review,['method'=>'post','route'=>['review.store']]) !!}
                        <div class="review-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    @if($user->company_logo)
                                      <img class="company-logo pull-right" src="{{ asset('uploads/'.$user->company_logo)}}" alt="Company Logo">
                                    @endif
                                </div>
                            </div>
                            <div class="row inside-address">
                                <div class="col-xs-12">
                                    <p class="date-created">{{  $default_review->date }}</p>
                                    <div class="form-group">
                                        {!! Form::email("client_email",old('client_email', $default_review->client_email),["class" => "review-input client-email" , "placeholder" => $default_review->client_email , "required" => "required"]) !!}
                                        @if ($errors->has('client_email'))
                                            <p class="error-msg">{{ $errors->first('client_email') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text("client_name",old('client_name', $default_review->client_name),["class" => "review-input client-name" , "placeholder" => $default_review->client_name , "required" => "required"]) !!}
                                        @if ($errors->has('client_name'))
                                            <p class="error-msg">{{ $errors->first('client_name') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text("client_company",old('client_company', $default_review->client_company),["class" => "review-input client-company" , "placeholder" => $default_review->client_company , "required" => "required"]) !!}
                                        @if ($errors->has('client_company'))
                                            <p class="error-msg">{{ $errors->first('client_company') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::textarea("client_address",old('client_address', $default_review->client_address),["class" => "review-input client-address review-textarea" , "rows" => 1 , "placeholder" => $default_review->client_address , "required" => "required"]) !!}
                                        @if ($errors->has('client_address'))
                                            <p class="error-msg">{{ $errors->first('client_address') }}</p>
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="review-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="project-details">
                                        <div class="form-group">
                                            {!! Form::text("project_title",old('project_title', $default_review->project_title),["class" => "review-input project-title" , "placeholder" => $default_review->project_title , "required" => "required"]) !!}
                                            @if ($errors->has('project_title'))
                                                <p class="error-msg">{{ $errors->first('project_title') }}</p>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            {!! Form::text("project_location",old('project_location', $default_review->project_location),["class" => "review-input project-location" , "placeholder" => $default_review->project_location , "required" => "required"]) !!}
                                            @if ($errors->has('project_location'))
                                                <p class="error-msg">{{ $errors->first('project_location') }}</p>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            {!! Form::text("project_iteration",old('project_iteration', $default_review->project_iteration),["class" => "review-input project-iteration" , "placeholder" => $default_review->project_iteration , "required" => "required"]) !!}
                                            @if ($errors->has('project_iteration'))
                                                <p class="error-msg">{{ $errors->first('project_iteration') }}</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text("salutation",old('salutation', $default_review->salutation),["class" => "review-input salutation" , "placeholder" => $default_review->salutation , "required" => "required"]) !!}
                                        @if ($errors->has('salutation'))
                                            <p class="error-msg">{{ $errors->first('salutation') }}</p>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::textarea("general_info",old('general_info', $default_review->general_info),["class" => "review-input general-info review-textarea" ,"rows" => 3 , "placeholder" => $default_review->general_info , "required" => "required"]) !!}
                                        @if ($errors->has('general_info'))
                                            <p class="error-msg">{{ $errors->first('general_info') }}</p>
                                        @endif
                                    </div>
                                    <div class="comment-container">
                                        {{ csrf_field() }}
                                        <ol class="comment-list">
                                            @if(old("comment"))
                                                @foreach(old("comment") as $comment)
                                                <li>
                                                    <textarea name="comment[]" class="review-input comment-item" rows="1" required="required">{{ $comment }}</textarea> <i class="fa fa-remove remove-comment-btn"></i>
                                                </li>
                                                @endforeach
                                            @endif
                                        </ol>
                                        <a href="#" class="add-comment-btn">add comment</a>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::textarea("closing_remarks",old('closing_remark', $default_review->closing_remark),["class" => "review-input closing-remark review-textarea" ,"rows" => 1 ,"placeholder" => $default_review->closing_remark]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="review-footer">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        {!! Form::text("complimentary_closing",old('complimentary_closing', $default_review->complimentary_closing),["class" => "review-input complimentary-closing" , "placeholder" => $default_review->complimentary_closing , "required" => "required"]) !!}
                                        @if ($errors->has('complimentary_closing'))
                                            <p class="error-msg">{{ $errors->first('complimentary_closing') }}</p>
                                        @endif
                                    </div>
                                    <p class="company-name">{{ $user->company }}</p>
                                    @if($user->electronic_signature)
                                        <img class="electronic-signature" src="{{ asset('uploads/'.$user->electronic_signature)}}" alt="Electronic Signature">
                                    @endif
                                    <p class="user-name">{{ $user->first_name." ".$user->last_name }}</p>
                                    <p class="user-title">{{ $user->credentials }}</p>
                                </div>
                            </div>
                            <div class="row project-footer">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        {!! Form::text("project_number",old('project_number', $default_review->project_number),["class" => "review-input project-number" , "placeholder" => $default_review->project_number ,"required" => "required"]) !!}
                                        @if ($errors->has('project_number'))
                                            <p class="error-msg">{{ $errors->first('project_number') }}</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <p class="user-phonenumber pull-right">{{ $user->phone_number }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="copyright">©{{ Date("Y") }} {{ $user->company }}  All Rights Reserved</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <button class="btn btn-primary create-review-btn">Create Review</button>
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/template" id="comment-container-tpl">
        <li>
            <textarea class="review-input comment-item" rows="1" required="required"></textarea> <i class="fa fa-remove remove-comment-btn"></i>
        </li>
    </script>
@endsection
