@extends("layouts.review_mail")

@section("email.footer")
    <tr>
        <td bgcolor="#ffffff">
            <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="100%">
                @include("review.partials.copyright")
            </table>
        </td>
    </tr>

@endsection
