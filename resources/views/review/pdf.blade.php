<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ config('app.name', 'Comment Complete') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <style>

        html {

            margin: 20px 40px 10px;
        }

        body {

            color: #333333;
            font-family: sans-serif;;
            font-size: 14px;
            font-weight: 400;

        }

        p {

            line-height: 20px;
            padding: 0;
            margin: 0 0 10px 0;

        }

        .company-logo , .electronic-signature {

            max-height: 100px;
            max-width: 200px;

        }

        .company-logo {

            float: right;
        }

        .client-email {

            margin:20px 0;
        }

        .project-details {

            font-weight: 700;
            margin: 20px 0 30px;
            text-transform: uppercase;
        }

        .salutation {

            margin: 0 0 20px;
        }

        .comment-list {

            margin: 0;
            padding: 0 0 0 40px;
        }

        .comment-list li{

            line-height: 20px;
            padding: 0 0 20px;

        }

        .company-name {

            font-weight: 700;
            text-transform: uppercase;
        }

        .project-number {

            float: left;
        }

        .user-phonenumber {

            float: right;
        }

        .user-title {

            margin: 0 0 40px;
        }

        .copyright {

            font-size: 12px;
        }


    </style>

</head>

<body>

    <section class="review-show-form-container">
        <header class="review-heading">
            <div>
                @if($user->company_logo)
                    <img class="company-logo" src="{{ asset('uploads/'.$user->company_logo)}}" alt="Company Logo">
                @endif
            </div>
            <div style="clear:both;"></div>

            <div class="inside-address">
                <p class="date-created">{{  $review->created_at->format("d F Y") }}</p>

                <p class="client-email">Via Email: {{ $review->client_email}}</p>

                <p class="client-name">{{ $review->client_name}}</p>

                <p class="client-company">{{ $review->client_company }}</p>

                <div class="client-address">
                    <?php $address = explode("<br />",nl2br($review->client_address)); ?>
                    @foreach($address as $value)
                        @if($value != "")
                            <p>{{ $value }}</p>
                        @endif
                    @endforeach
                </div>
            </div>
        </header>
        <div class="review-body">
            <div class="project-details">
                <p class="project-title">{{ $review->project_title }}</p>
                <p class="project-location">{{ $review->project_location }}</p>
                <p class="project-iteration">{{ $review->project_iteration }}</p>
            </div>
            <p class="salutation">{{ $review->salutation }}</p>
            <?php $general_info = explode("<br />",nl2br($review->general_info)); ?>
            @foreach($general_info as $value)
                @if($value != "")
                    <p class="general_info">{{ $value }}</p>
                @endif
            @endforeach
            <ol class="comment-list">
                @foreach($review->comments()->get() as $comment)
                    <li>{{$comment->comment}}</li>
                @endforeach
            </ol>
            <p class="closing-remarks">{{ $review->closing_remarks }}</p>
        </div>
        <div class="review-footer">

            <p class="complimentary-closing">{{ $review->complimentary_closing }}</p>
            <p class="company-name">{{ $user->company }}</p>
            @if($user->electronic_signature)
                <img class="electronic-signature" src="{{ asset('uploads/'.$user->electronic_signature)}}" alt="Electronic Signature">
            @endif
            <p class="user-name">{{ $user->first_name." ".$user->last_name }}</p>
            <p class="user-title">{{ $user->credentials }}</p>
            <div>
                <p class="project-number">{{ $review->project_number }}</p>
                <p class="user-phonenumber">{{ $user->phone_number }}</p>
            </div>
            <div style="clear:both;"></div>
            <p class="copyright">©{{ Date("Y") }} {{ $user->company }}  All Rights Reserved</p>
        </div>
    </section>

</body>

</html>
