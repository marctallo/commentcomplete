@extends("layouts.review_mail")

@section("email.style")
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
@endsection

@section("email.footer")
    <tr>
        <td bgcolor="#ffffff">
            <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="100%">
                @include("review.partials.copyright")
                <tr>
                    <td style="padding: 20px 40px; font-family: sans-serif; font-size: 12px; line-height: 20px; background:#3097D1;">
                        <ul class="option-list">
                            <li>
                                <a href="{{ route('review.index') }}"><i class="fa fa-arrow-left"  data-toggle="tooltip" title="Back"></i></a>
                            </li>
                            <li>
                                <a href="{{ route('review.edit',$review) }}"><i class="fa fa-edit"  data-toggle="tooltip" title="Edit"></i></a>
                            </li>
                            <li>
                                @if($review->hasClientEmail())
                                    <a href="{{ route('review.email',$review) }}" class="send-review-mail-btn"><i class="fa fa-send-o"  data-toggle="tooltip" title="Send Email"></i></a>
                                    <span class="sr-only">Loading...</span>
                                @else
                                    <i class="fa fa-send" data-toggle="tooltip" title="No email recepient"></i>
                                @endif

                            </li>
                            <li>
                                <a href="{{ route('download.review.pdf' , $review) }}"><i class="fa fa-file-pdf-o" data-toggle="tooltip" title="Download PDF"></i></a>
                            </li>
                            <li>
                                {!! Form::open(['method'=>'delete','route'=>['review.destroy',$review],'class'=>'delete-form','data-modal-msg' => 'Delete this review?','data-modal-title' => 'Delete']) !!}
                                    <a href="#" ><i class="fa fa-trash delete-form-btn" data-toggle="tooltip" title="Delete Review"></i></a>
                                {!! Form::close() !!}
                            </li>

                        </ul>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

@endsection

@section("email.script")
    <script src="{{ asset('js/app.js') }}"></script>
@endsection
