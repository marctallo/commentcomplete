@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            @include('page.partials.notification_container')
            <div class="content-container review-list-container">
                <h2>My Reviews </h2>
                <hr>
                <a href="{{ route('review.create') }}" class="btn btn-primary btn-sm pull-right new-review-btn">New Review</a>
                <div class="table-container">
                    <table class="table table-bordered review-list-table">
                      <thead>
                          <tr>
                              <th>Project Name</th>
                              <th>Review Iteration</th>
                              <th>Client</th>
                              <th>Date Created</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody>
                          @if($reviews->count() != 0)
                            @foreach($reviews as $review)
                                <tr>
                                    <td>{{ $review->project_title }}</td>
                                    <td>{{ $review->project_iteration }}</td>
                                    <td>{{ $review->client_company }}</td>
                                    <td>{{ $review->created_at->format("d F Y") }}</td>
                                    <td>

                                        <ul class="option-list">
                                            <li>
                                                <a href="{{ route('review.show',$review) }}"><i class="fa fa-eye" data-toggle="tooltip" title="Preview"></i></a>
                                            </li>
                                        </ul>

                                    </td>
                                </tr>
                            @endforeach
                          @else
                          <tr>
                              <td colspan="5">
                                  <small class="text-center"><p class="no-record-text">You have no reviews.</p></small>
                              </td>
                          </tr>
                          @endif
                      </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
