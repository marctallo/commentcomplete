@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <div class="content-container">
                    <section class="review-show-form-container">
                        <header class="review-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    @if($user->company_logo)
                                        <img class="company-logo pull-right" src="{{ asset('uploads/'.$user->company_logo)}}" alt="Company Logo">
                                    @endif
                                </div>
                            </div>
                            <div class="row inside-address">
                                <div class="col-xs-12">
                                    <p class="date-created">{{  $review->created_at->format("d F Y") }}</p>

                                    @if($review->client_email && $review->client_email != "")
                                        <p class="client-email">Via Email: {{ $review->client_email}}</p>
                                    @endif

                                    <p class="client-name">{{ $review->client_name}}</p>

                                    <p class="client-company">{{ $review->client_company }}</p>

                                    <address class="client-address">
                                        <?php $address = explode("<br />",nl2br($review->client_address)); ?>
                                        @foreach($address as $value)
                                            @if($value != "")
                                                <p>{{ $value }}</p>
                                            @endif
                                        @endforeach
                                    </address>

                                </div>
                            </div>
                        </header>
                        <div class="review-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="project-details">
                                        <p class="project-title">{{ $review->project_title }}</p>
                                        <p class="project-location">{{ $review->project_location }}</p>
                                        <p class="project-iteration">{{ $review->project_iteration }}</p>
                                    </div>
                                    <p class="salutation">{{ $review->salutation }}</p>
                                    <?php $general_info = explode("<br />",nl2br($review->general_info)); ?>
                                    @foreach($general_info as $value)
                                        @if($value != "")
                                            <p class="general_info">{{ $value }}</p>
                                        @endif
                                    @endforeach
                                    <ol class="comment-list">
                                        @if(count($review->comments) > 0)
                                            @foreach($review->comments as $comment)
                                                <li>{{$comment->comment}}</li>
                                            @endforeach
                                        @endif
                                    </ol>
                                    <p class="closing-remarks">{{ $review->closing_remarks }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="review-footer">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="complimentary-closing">{{ $review->complimentary_closing }}</p>
                                    <p class="company-name">{{ $user->company }}</p>
                                    @if($user->electronic_signature)
                                        <img class="electronic-signature" src="{{ asset('uploads/'.$user->electronic_signature)}}" alt="Electronic Signature">
                                    @endif
                                    <p class="user-name">{{ $user->first_name." ".$user->last_name }}</p>
                                    <p class="user-title">{{ $user->credentials }}</p>
                                </div>
                            </div>
                            <div class="row project-footer">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <p>{{ $review->project_number }}</p>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <p class="user-phonenumber pull-right">{{ $user->phone_number }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="copyright">©{{ Date("Y") }} {{ $user->company }}  All Rights Reserved</p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
