//commont page functions

let Form = {

    init : function(){

        this.setupSubmitForm();
    },

    setupSubmitForm : function(){

        let delete_form_btn = $(".delete-form-btn"),
            delete_form = $(".delete-form");

        delete_form_btn.click(function(e){

            e.preventDefault();

            let self = $(this),
                form = self.parents(".delete-form");

            form.submit();
        });

        delete_form.on("submit",this.showModal);

    },
    showModal : function(e){

        e.preventDefault();

        let self = $(this),
            msg = self.attr("data-modal-msg"),
            title = self.attr("data-modal-title");

        Bootbox.confirm({
            title: title,
            message: msg,
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel',
                    className : 'btn-sm btn-default'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm',
                    className : 'btn-sm btn-success'
                }
            },
            callback: function (result) {

                if (result) {

                    self.unbind().submit();
                }
            }
        });


    }
}

let Notification = {

    init : function(){

        window.setTimeout(function() {

            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 4000);
    }
}

let UploadButton = {

    init : function(){

        let file_upload_btn = $(".file-upload-btn");

        file_upload_btn.on("change",function(){

            let self = $(this),
                file_upload_text = self.prev(),
                text = self.val();

            file_upload_text.text(text.split(/(\\|\/)/g).pop());
        });
    }
}

let Common = {

    init : {

        init : function(){

            Form.init();

            Notification.init();

            $('[data-toggle="tooltip"]').tooltip();

            UploadButton.init();
        }
    }
}

module.exports = Common.init;
