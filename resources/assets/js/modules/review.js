var Review = {

    init : {

        init : function(){

            Review.setUpTextAreaPlugins();

            Review.setupEvents();
        }
    },

    setUpTextAreaPlugins : function(){

        //add autosize on other textearea beside comments textarea
        Autosize($(".review-textarea"));

        this.setTextAreaFocus();

        this.setCommentTextAreaPlugin();

    },

    setTextAreaFocus : function(){

        $(".review-input").focus(function() { $(this).select(); } );
    },

    setCommentTextAreaPlugin : function(){

        $(".comment-item").each(function(){

            let self = $(this);

            Review.setupPlugins(self);
        });
    },

    setupPlugins : function(textarea){

        Autosize(textarea);

        let options = {

           url: function(phrase) {

                 return $("#base-url").val()+"/comment/list";
           },
           getValue: function(element) {

                return element.comment;
           },
           ajaxSettings: {
                dataType: "json",
                method: "POST",
                data : {

                    _token : $("input[name='_token']").val()
                }
          },
          preparePostData: function(data) {
               data.phrase = textarea.val();
               return data;
          },
          adjustWidth: false,
          requestDelay: 200,
          list : {

              onClickEvent: function() {

                 Autosize.update(textarea);
             },
             maxNumberOfElements: 15,
          }
       }

       textarea.easyAutocomplete(options)
    },

    setupEvents : function(){

        let add_comment_btn = $(".add-comment-btn"),
            send_review_mail_btn = $(".send-review-mail-btn"),
            comment_container = $(".comment-container");

        add_comment_btn.click(this.addCommentRow);

        comment_container.on("click",".remove-comment-btn",this.deleteCommentRow);

        send_review_mail_btn.click(this.changeSendReviewEmailButton);
    },

    changeSendReviewEmailButton : function(){

        let self = $(this),
            icon = self.find("i.fa");

        icon.removeClass("fa-send-o").addClass("fa-spinner fa-pulse");

    },

    addCommentRow : function(e){

        e.preventDefault();

        let comment_list = $('.comment-list');

        comment_list.append($('#comment-container-tpl').html());

        let added_comment_box = comment_list.find(".comment-item").last();

        added_comment_box.attr("name","comment[]");

        Review.setupPlugins(added_comment_box);
    },

    deleteCommentRow : function(){

        let self = $(this),
            parent = self.parents("li");

            parent.remove();
    }


}

module.exports = Review.init;
