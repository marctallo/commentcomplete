
window._ = require('lodash');

window.Autosize = require('autosize');

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap-sass');

} catch (e) {}

window.EasyAutocomplete = require('easy-autocomplete');

window.Bootbox = require('bootbox');

window.Common = require('./modules/common');

window.Review = require('./modules/review');
